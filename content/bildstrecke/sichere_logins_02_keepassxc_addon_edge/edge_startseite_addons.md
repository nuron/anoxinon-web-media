---
title: KeePassXC für Edge suchen 
description: Die Addonverwaltung bei Edge lässt sich über die drei Punkte oben rechts → Erweiterungen öffnen.
image: /img/sichere_logins_02/edge_keepassxc_01_e.png
weight: 1
---
