---
title: KeePassXC für Edge suchen 
description: Dort kann man über "Erweiterungen für Microsoft Edge abrufen" neue Addons suchen und installieren.
image: /img/sichere_logins_02/edge_keepassxc_02_e.png
weight: 2
---
