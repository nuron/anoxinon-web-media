---
title: KeePassXC installieren 
description: Für die Installation braucht KeePassXC Admin-Rechte, dafür den aufploppenden Dialog mit "Ja" bestätigen. 
image: /img/sichere_logins_02/10_win_keepassxc_confirm_win_install_e.png
weight: 11
---
