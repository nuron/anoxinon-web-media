---
title: KeePassXC herunterladen 
description: Auf der Download-Seite findet man KeePassXC zum Herunterladen, hier ist in aller Regel der "MSI Installer" ganz oben anzuwählen.
image: /img/sichere_logins_02/02_win_keepassxc_webseite_download_e.png
weight: 2
---
