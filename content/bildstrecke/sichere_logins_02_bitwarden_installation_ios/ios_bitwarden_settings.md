---
title: Bitwarden einrichten  
description: Bitwarden kann Deine Zugangsdaten in Apps automatisch eintragen. Diese Funktion musst Du manuell aktivieren, wähle daher die Bitwarden-Einstellungen und "Passwort AutoFill", um Dir zeigen zu lassen, wie Du das bewerkstelligen kannst. 
image: /img/sichere_logins_02/ios_bitwarden_firststart_05_e.png
weight: 12
---
