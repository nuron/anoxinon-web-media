---
title: Bitwarden ist installiert 
description: Fertig - Bitwarden ist installiert! Nun fahre mit der Einrichtung fort, klicke dazu auf "Öffnen" und starte Bitwarden. 
image: /img/sichere_logins_02/ios_appstore_bitwarden_installed_e.png
weight: 7
---
