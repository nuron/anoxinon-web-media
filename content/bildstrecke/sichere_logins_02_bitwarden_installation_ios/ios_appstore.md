---
title: Bitwarden im AppStore suchen 
description: Navigiere als nächstes zum Suchbereich des AppStores und starte eine neue Suche durch das Auswählen der Suchleiste. 
image: /img/sichere_logins_02/ios_appstore_e.png
weight: 2
---
