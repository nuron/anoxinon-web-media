---
title: Bitwarden einrichten  
description: Schalte hier den Toggle für Autofill an und wähle Bitwarden als AutoFill-App aus. Am Ende sollte es wie im Bildschirmfoto zu sehen aussehen. 
image: /img/sichere_logins_02/ios_bitwarden_firststart_08_e.png
weight: 17
---
