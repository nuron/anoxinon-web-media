---
title: Bitwarden installieren 
description: Schlussendlich wird Bitwarden installiert, der Fortschritt lässt sich am Ladekreis ablesen. Unter Umständen musst Du die Installation durch Eingabe Deines Apple-ID-Passwortes, der Touch- oder Face-ID bestätigen. 
image: /img/sichere_logins_02/ios_appstore_bitwarden_installprocess_e.png
weight: 6
---
