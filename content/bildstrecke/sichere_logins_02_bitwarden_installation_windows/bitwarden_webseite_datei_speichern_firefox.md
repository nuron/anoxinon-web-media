---
title: Bitwarden speichern 
description: Anschließend kann Bitwarden heruntergeladen werden, dafür muss man "Datei speichern" auswählen.
image: /img/sichere_logins_02/03_win_bitwarden_webseite_safe_file_e.png
weight: 3
---
