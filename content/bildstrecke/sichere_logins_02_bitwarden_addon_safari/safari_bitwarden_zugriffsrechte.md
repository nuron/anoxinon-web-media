---
title: Bitwarden im Safari aktivieren 
description: Nun befindet sich das Addon in der Safari-Symbolleiste, dort fragt es noch nach Berechtigungen. Diese werden benötigt, damit Bitwarden die Logindaten auf Webseiten ausfüllen kann. Dir steht es frei, die Berechtigungen nach Deinem belieben zu gewähren, unter Umständen musst Du zu späterem Zeitpunkt noch weitere gewähren, wenn Du sie anfangs zu restriktiv gewählt hast. 
image: /img/sichere_logins_02/03_safari_bitwarden_berechtigungen.png
weight: 3
---
