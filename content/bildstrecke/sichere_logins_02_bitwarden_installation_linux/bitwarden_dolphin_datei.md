---
title: Bitwarden Programmdatei 
description: Nun ist die Programmdatei gespeichert, man kann sie bei Bedarf noch in einen anderen Ordner verschieben. Über einen Rechtsklick lassen sich die Datei-Eigenschaften öffnen. Das Aussehen kann je nach der eigesetzten Linux-Variante bei Dir variieren, die Namensgebung sollte grundlegend änhlich sein.
image: /img/sichere_logins_02/bitwarden_dolphindatei_e.png
weight: 4
---
