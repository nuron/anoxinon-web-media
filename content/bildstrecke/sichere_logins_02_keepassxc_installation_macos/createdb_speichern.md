---
title: KeePassXC einrichten 
description: Schlussendlich musst Du die Passwortdatei abspeichern. Veliere sie keinesfalls und sichere sie regelmäßig, ohne sie kommst Du nicht mehr an Deine gespeicherten Zugangsdaten. 
image: /img/sichere_logins_02/16_macos_keepassxc_createdb_safe.png
weight: 17
---
