---
title: Bitwarden für Chrome installieren 
description: Chrome macht Dich noch auf die Berechtigungen, die Bitwarden benötigt, aufmerksam. Nach einem weiteren Klick auf "Erweiterung hinzufügen" ist das Addon installiert.
image: /img/sichere_logins_02/chrome_bitwarden_05_e.png
weight: 5
---
