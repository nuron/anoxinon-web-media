---
title: Bitwarden für Chrome suchen 
description: Im Webstore gibt es ein Suchfeld, in dem Du den Addon-Katalog von Google Chrome durchsuchen kannst. In unserem Fall gibst Du hier "Bitwarden" ein und drückst Enter.
image: /img/sichere_logins_02/chrome_bitwarden_02_e.png
weight: 2
---
