---
title: Bitwarden anmelden 
description: Ab sofort findest Du oben in Deiner Chrome-Leiste ein kleines Puzzleteil-Symbol. Darunter ist Dein Bitwarden-Addon gelistet, das siehst Du nach einem Klick.  Um es einfacher zugänglich zu machen, klicke auf das Pinnadel-Symbol, um Bitwarden an die Chrome-Leiste anzupinnen.
image: /img/sichere_logins_02/chrome_bitwarden_08_e.png
weight: 9
---
