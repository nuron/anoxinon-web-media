---
title: KeePassXC für Chrome installieren 
description: Nachfolgend kannst Du das Addon einfach mit Klick auf "Hinzufügen" installieren.
image: /img/sichere_logins_02/chrome_keepassxc_04_e.png
weight: 4
---
