---
title: KeePassXC-Addon mit KeePassXC verbinden 
description: Nun kannst Du das KeePassXC-Symbol in der oberen Leiste anwählen. Wenn Du Chrome nicht in der Zwischenzeit neu gestartet hast, musst Du dort auf "Neu laden" klicken, damit das Addon Dein KeePassXC-Programm erkennen kann. Beachte allerdings, dass Dein KeePassXC-Passwortspeicher dazu entsperrt sein muss - sonst passiert nichts!!! (Standardmäßig sperrt sich KeePassXC nach einer Zeit von selbst wieder.) Wenn nichts passiert - das gilt auch für die folgenden Schritte - dann öffne das Addon einmal neu!
image: /img/sichere_logins_02/chrome_keepassxc_11_e.png
weight: 11
---
