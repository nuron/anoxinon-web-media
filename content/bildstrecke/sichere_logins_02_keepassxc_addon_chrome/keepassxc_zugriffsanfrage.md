---
title: KeePassXC-Addon mit KeePassXC verbinden 
description: In der folgenden Verbindungsanfrage kannst Du nun schlussendlich dem KeePassXC-Addon Zugriff auf Deinen Passwortspeicher geben. Dafür kannst Du der Verbindung einen Namen geben, zum Beispiel "Mein Chrome" oder etwas anderes Deiner Wahl und das Setup mit Klick auf "Speichern und Zugriff erlauben" abschließen.
image: /img/sichere_logins_02/chrome_keepassxc_13_e.png
weight: 13
---
