---
title: Strongbox Passwortdatei anlegen
description: Dieser Schritt ist super wichtig, hier musst Du das Masterpasswort für die Passwortdatei wählen. Suche hier unbedingt ein sicheres Passwort - damit werden alle anderen Deiner Zugangsdaten, die Du in Strongbox speicherst, geschützt! Genauso solltest Du es Dir aber merken, denn wenn Du es vergisst, wirst Du nicht mehr an die im Passwortmanager gespeicherten Zugänge herankommen! 
image: /img/sichere_logins_02/ios_strongbox_firststart_14_e.PNG
weight: 24
---
