---
title: Strongbox entsperren 
description: Vorher haben wir Autofill für Strongbox aktiviert. Nun müssen wir es noch für unsere Passwortdatei aktivieren. Dafür wählen wir "Automat. Ausfüllen verwenden".
image: /img/sichere_logins_02/ios_strongbox_firststart_18_e.PNG
weight: 28
---
