---
title: Strongbox im AppStore suchen 
description: Navigiere als nächstes zum Suchbereich des AppStores und starte eine neue Suche durch Anwählen der Suchleiste. 
image: /img/sichere_logins_02/ios_appstore_e.png
weight: 2
---
