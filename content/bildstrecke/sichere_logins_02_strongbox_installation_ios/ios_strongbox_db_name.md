---
title: Strongbox Passwortdatei anlegen
description: Strongbox fordert uns auf, einen Namen für die Passwortdatei zu wählen. Suche Dir etwas beliebiges aus und fahre mit "Weiter" fort. 
image: /img/sichere_logins_02/ios_strongbox_firststart_13_e.PNG
weight: 23
---
