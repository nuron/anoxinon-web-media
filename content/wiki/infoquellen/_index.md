---
title: Infoquellen
layout: wikipage
---

## Bücher 

- "Die Daten, die ich rief" von Katharina Nocun  
- "Kultur der Digitalität" von Felix Stadler  
- "Wie künstliche Intelligenz uns berechnet, steuert und unser Leben verändert" von Ki Schlietern  
- "Qualityland" von Marc-Uwe Kling  
- "Eine kurze Geschichte der Digitalisierung" von Martin Burkhard  
- "Zehn Gründe, warum du deine Social Media Accounts sofort löschen musst" von Jaron Lanier  
- "Jäger, Hirten, Kritiker" von Richard David Precht  
- "Die Kunst der Täuschung" von Kevin D. Mitnick  
- "Der Tag, an dem Oma das Internet kaputt gemacht hat" von Marc Uwe-Kling

## TV

- [Prism is a dancer](https://zdf.de/show/lass-dich-ueberwachen/lass-dich-ueberwachen-die-prism-is-a-dancer-show-vom-2-november-2018-100.html)
- [Im Rausch der Daten](https://ardmediathek.de/tv/Filme-im-Ersten/Dokumentarfilm-im-Ersten-Im-Rausch-der-/Das-Erste/Video?bcastId=1933898&documentId=52620920)  
- [Leben nach Microsoft (Achtung Youtube Video)](https://youtube.com/watch?v=V3rGn-PIX_s)  
- [Reportage über F-droid](https://zdf.de/nachrichten/heute/zehn-freundliche-android-apps-100.html)  

## Hörspiel und Radio

- [Tom Schimmeck - Silicon Dreams](https://wdr.de/radio/wdr3/programm/sendungen/wdr3-hoerspiel/download-tom-schimmeck-silicon-dreams-100.html) Hörspiel  
- [Herrschaft der Algorithmen](https://swr.de/swr2/programm/sendungen/wissen/herrschaft-der-algorithmen/-/id=660374/did=21528068/nid=660374/1orqyjs/index.html) SWR Radio

## HowTos

### Bootmedien

- [USB Sticks mit .iso Bootimage erstellen](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-ubuntu) 

### AFWall+

- [AFWall+-Skripte](https://kuketz-blog.de/afwall-digitaler-tuervorsteher-take-back-control-teil4/)

## Linux-Wikis

- [Arch Linux Wiki](https://wiki.archlinux.org/index.php/list_of_applications)
- [Ubuntuusers](https://ubuntuusers.de/)  
