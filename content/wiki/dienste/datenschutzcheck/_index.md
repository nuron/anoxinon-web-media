---
title: Datenschutz prüfen
layout: wikipage
---

Folgende Tools können verwendet werden, um den Datenschutz von Apps und Services zu prüfen, eine richtige Interpretation der Ergebnisse vorausgesetzt.

## Webseiten

- [Privacyscore](https://privacyscore.org) - Prüft Webseiten auf Datenschutz (deutsches Projekt) 
- [TOSDR](https://tosdr.org/) - Bewertet die AGBs von Webseiten und Apps auf Verbraucherfreundlichkeit 
- [Webkoll](https://webbkoll.dataskydd.net) - Prüft Webseiten auf Sicherheit und Datenschutz, unter anderem auf Tracker  

## Apps

- [Exodus Privacy](https://reports.exodus-privacy.eu.org/de/) - Scannt Android-Apps auf Tracker
- [Mobilsicher AppChecker](https://appcheck.mobilsicher.de/) - App-Liste des Mobilsicher-Teams über das Datensendeverhalten von Android und iOS-Apps
