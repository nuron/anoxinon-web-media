---
title: Git-Dienste - GitHub Alternativen
layout: wikipage
---

Für quelloffene und datenschutzfreundliche Software bietet es sich an, die Entwicklung und Veröffentlichung auf einer Git-Plattform zu handhaben, die den gleichen Kriterien entspricht - also nicht GitHub, sonder bspw. auf Gitea-Instanzen.

## Gitea

- [Codeberg](https://codeberg.org) - ein Git-Hosting Angebot des Vereins Codeberg e.V. ([Datenschutzerklärung](https://codeberg.org/codeberg/org/src/branch/master/PrivacyPolicy.md), [Verantwortliche:r](https://codeberg.org/codeberg/org/src/branch/master/Imprint.md))
- [Disroot Git](https://disroot.org/de/services/git) - Gitea-Instanz von Disroot  ([Datenschutzerklärung](https://disroot.org/de/privacy_policy), [Verantwortliche:r](https://disroot.org/de/privacy_policy))
- [SP-Codes Gitea](https://sp-codes.de/de/services/gitea/) - Gitea-Angebot von SP-Codes  ([Datenschutzerklärung](https://sp-codes.de/de/privacy), [Verantwortliche:r](https://sp-codes.de/de/imprint/))

## Gogs

- [NotABug](https://notabug.org) - Gogs-Instanz der Peers Community  ([Datenschutzerklärung](https://notabug.org/tos), [Verantwortliche:r](https://notabug.org/about))

Alternativ gibt es die Möglichkeit Gitea oder Gitlab, selbst zu hosten bzw. anderen öffentlichen Instanzen beizutreten.
