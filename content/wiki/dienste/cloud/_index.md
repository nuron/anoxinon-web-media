---
title: Cloud bzw. Onlinespeicher
layout: wikipage
---

## Dateicloud

Bei der Erwähnung von Cloud fallen vielen Gesprächspartnern automatisch Namen wie "iCloud", "Dropbox" & "Google Cloud" ein.
Es gibt genug Gründe, die gegen diese Anbieter:innen sprechen. Die Digitalcourage hat hier schon [einen guten Beitrag](https://digitalcourage.de/digitale-selbstverteidigung/alternativen-zu-dropbox-und-cloud) geschrieben.

Am besten hostet man seine Cloud selbst, z. B. mit [Nextcloud](https://nextcloud.com) oder [Seafile](https://www.seafile.com).  
Man sollte seinen Daten zusätzlich verschlüsseln z. B. mit [Cryptomator](https://cryptomator.org/) oder die Ende-zu-Ende Verschlüsselung von Nextcloud/Seafile verwenden.

Alternativ schaut man sich im Internet nach Nextcloud/Seafile-Instanzen um oder fragt Bekannten mit mehr technischem Wissen. Ein paar Beispiele nenne wir nachfolgend.

### Nextcloud-Instanzen

- [Disroot Cloud](https://disroot.org/de/services/nextcloud) - Von dem niederländischen Kollektiv Disroot verwaltete Nextcloud ([Datenschutzerklärung](https://disroot.org/de/privacy_policy), [Verantwortliche:r](https://disroot.org/de/privacy_policy))
- [StorageShare](https://www.hetzner.com/de/storage/storage-share) - Von Hetzner verwaltete Nextloud ([Datenschutzerklärung](https://www.hetzner.com/de/rechtliches/datenschutz), [Verantwortliche:r](https://www.hetzner.com/de/rechtliches/impressum))
- [Systemli Cloud](https://www.systemli.org/service/cloud/) - Nextcloud-Instanz von dem linken Technik-Kollektiv Systemli ([Datenschutzerklärung](https://www.systemli.org/tos/), [Verantwortliche:r](https://www.systemli.org/support-us/))

### Dateiclouds zum Kaufen

**Info:** Die folgenden Systeme dienen nur als Vorschlag, sie wurden nicht von uns getestet.

- [Freedombox](https://www.freedombox.org/) - Per Webbrowser verwaltbarer Homeserver änhlich zu modernen NAS-Systemen von Synology oder Qnap
- [Nextbox](https://shop.nitrokey.com/de_DE/shop/product/nextbox-116) - Vorkonfigurierter und wartungsarmer Nextcloud-Server von Nitrokey


## Kollaboratives Arbeiten

### Cryptpad

Um im Team gemeinsam Texte zu verfassen eignet sich Cryptpad.

- [cryptpad.fr](https://cryptpad.fr) - Offizielle Cryptpad Instanz ([Datenschutzerklärung](https://cryptpad.fr/privacy.html), [Verantwortliche:r](https://cryptpad.fr/what-is-cryptpad.html))
- [Disroot Cryptpad](https://disroot.org/de/services/cryptpad) - Cryptpad-Instanz des niederländischen Kollektivs Disroot ([Datenschutzerklärung](https://disroot.org/de/privacy_policy), [Verantwortliche:r](https://disroot.org/de/privacy_policy))

Alternativ kann man Cryptpad selber betreiben oder eine andere Instanz suchen.

Bitte beachten, dass man keine sensiblen Daten in Pads speichern sollte, da diese auch ggf. anderen zugänglich gemacht werden könnten und was dort einmal eingetragen wurde bleibt für immer in der Dokument-History.

### HedgeDoc

Als kollaboratives Notiz-Tool bzw. Markdown-Editor eignet sich [HedgeDoc](https://hedgedoc.org/) (ehemals CodiMD).

- [Snoypta HedgeDoc](https://pad.snopyta.org/) - HedgeDoc-Instanz von Snopyta ([Datenschutzerklärung](https://snopyta.org/privacy_policy/), [Verantwortliche:r](https://snopyta.org/legal/))

### Etherpad und Ethercalc

Etherpad und Etherpad sind beides Open Source Webdienste. Bei ersterem handelt es sich um eine kollaborative Textverarbeitung, bei letzterem um eine kollaborative Tabellenkalkulation.

- [Disroot Pad](https://disroot.org/de/services/pads) - Etherpad-Instanz von Disroot ([Datenschutzerklärung](https://disroot.org/de/privacy_policy), [Verantwortliche:r](https://disroot.org/de/privacy_policy))
- [Disroot Calc](https://disroot.org/services/pads) - Ethercalc-Instanz von Disroot ([Datenschutzerklärung](https://disroot.org/de/privacy_policy), [Verantwortliche:r](https://disroot.org/de/privacy_policy))
- [Libreops Etherpad](https://pad.libreops.cc/) - Etherpad-Instanz von LibreOps ([Datenschutzerklärung](https://libreops.cc/terms.html), [Verantwortliche:r](https://libreops.cc/about/))
- [Snopyta Etherpad](https://etherpad.snopyta.org/) - Etherpad-Instanz von Snopyta ([Datenschutzerklärung](https://snopyta.org/privacy_policy/), [Verantwortliche:r](https://snopyta.org/legal/))
- [Snopyta Ethercalc](https://ethercalc.snopyta.org/) - Ethercalc-Instanz von Snopyta ([Datenschutzerklärung](https://snopyta.org/privacy_policy/), [Verantwortliche:r](https://snopyta.org/legal/))
- [Systemli Pad](https://www.systemli.org/service/etherpad/) - Etherpad-Instanz von Systemli, einem linken Technik-Kollektiv ([Datenschutzerklärung](https://www.systemli.org/tos/), [Verantwortliche:r](https://www.systemli.org/support-us/))
- [Systemli Calc](https://www.systemli.org/service/ethercalc/) - Ethercalc-Instanz von Systemli ([Datenschutzerklärung](https://www.systemli.org/tos/), [Verantwortliche:r](https://www.systemli.org/support-us/))
- [yopad](https://yopad.eu/) - Etherpad-Instanz der Arbeitsgemeinschaft Deutscher Bundesjugendring ([Datenschutzerklärung](https://blog.barcamptools.eu/datenschutzerklaerung-yourpart-eu/), [Verantwortliche:r](https://yopad.eu/))
