---
title: E-Mail Anbieter:innen
layout: wikipage
--- 

## E-Mail-Anbieter:innen

- [mailbox.org](https://mailbox.org) - Deutscher E-Mail-Provider mit Tarifen ab 1€ ([Datenschutzerklärung](https://mailbox.org/de/datenschutzerklaerung), [Verantwortliche:r](https://mailbox.org/de/impressum))
- [Posteo](https://posteo.de) - Deutscher E-Mail-Provider mit Traifen ab 1€ ([Datenschutzerklärung](https://posteo.de/site/datenschutzerklaerung), [Verantwortliche:r](https://posteo.de/site/datenschutzerklaerung))
- [Tutanota](https://tutanota.com/) *1 - E-Mail Provider mit eigenentwickelter Ende-zu-Ende-Verschüsselung ([Datenschutzerklärung](https://tutanota.com/de/privacy), [Verantwortliche:r](https://tutanota.com/de/imprint))

**Anmerkungen:**

**\*1** Tutanota ist zwar datenschutzfreundlich und sicher, aber weder zu OpenPGP noch zu anderen E-Mail-Programmen kompatibel.

**Info**

E-Mail-Anbieter, die mit "[Email made in Germany](https://media.ccc.de/v/30C3_-_5210_-_de_-_saal_g_-_201312282030_-_bullshit_made_in_germany_-_linus_neumann)" (GMX, Web.de, Freemail etc.) werben, zählen für uns **nicht** zu den Pionieren in Sachen Datenschutz. Ein paar Aspekte dazu, warum wir so denken, kann man neben dem eben verlinkten Video auch im [Kuketz-Blog nachlesen](https://www.kuketz-blog.de/e-mail-anbieter-verabschiedet-euch-von-gmx-web-de-und-weiteren-freemail-werbeplattformen/).  

## E-Mail managed Hosting

Neben den klassischen E-Mail-Anbieter:innen gibt es auch die Möglichkeit, sich bei vielen Hosting-Anbieter:innen eine E-Mail-Instanz mit mehr Einstellungsmöglichkeiten bis hin zu Quasi-Adminrechten anmieten.

- [Mailcow bei Servercow](https://servercow.de/mailcow#hosted) - Mit der klassischen Mailcow und der managed Mailcow gibt es zwei managed E-Mail-Angebote von Servercow, einem kleinen, deutschen Hoster ([Datenschutzerklärung](https://servercow.de/impressum), [Verantwortliche:r](https://servercow.de/impressum))
- [Uberspace](https://uberspace.de) - Uberspace bietet virtuelle Server im Pay-as-you-can-Prinzip mit einem vorkonfigurierten E-Mail-Server mit eingescränkter Verwaltung über ein einfaches Kommandozeilenprogramm ([Datenschutzerklärung](https://uberspace.de/de/about/privacy/), [Verantwortliche:r](https://uberspace.de/de/about/imprint/))

Alternativ kann man auch einen E-Mail-Server selbst betreiben, entweder als Gesamtpaket mit der [Mailcow](https://mailcow.email/) oder ganz klassisch mit Postfix und Dovecot.


