---
title: Dateisynchronisation
layout: wikipage
--- 

Bitte beachtet, dass eine Synchronisation kein Ersatz für Backups ist - wenn ihr etwas löscht und es überall gleichzeitig verschwindet hat euch die Synchronisation nicht geholfen.

- [Syncthing (Windows, MacOS, Linux)](https://syncthing.net) 
- [Nextcloud (Windows, MacOS, Linux)](https://nextcloud.com/clients) 
