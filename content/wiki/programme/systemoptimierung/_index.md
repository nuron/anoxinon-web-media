---
title: Systemoptimierung
layout: wikipage
--- 

## Akku

- [Battery Charge Limit (Android)](https://f-droid.org/en/packages/com.slash.batterychargelimit/)  

## Tastatur

- [Anysoftkeyboard (Android)](https://search.f-droid.org/?q=Anysoftkey&lang=en)  
