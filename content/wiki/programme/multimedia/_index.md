---
title: Multimedia
layout: wikipage
--- 

## Videospieler

- [VLC (Windows, Linux, MacOS, Android, iOS, Apple TV](https://videolan.org)
- [SmPlayer (Windows, Linux)](https://smplayer.info)  

## Audiospieler

- [VLC (Windows, Linux, MacOS, Android, iOS, Apple TV](https://videolan.org)
- [Banshee (Windows, Linux, MacOS)](https://banshee.fm)
- [Amarok (Windows, Linux)](https://amarok.kde.org)  
- [VanillaMusic (Android)](https://f-droid.org/en/packages/ch.blinkenlights.android.vanilla/) 

## Audiokonverter

- [Asunder (Linux)](https://wiki.ubuntuusers.de/Asunder)  

## Bildschirmaufnahme und Live-Streaming

- [OBS Studio (Windows, MacOS, Linux)](https://obsproject.com)
- [SimpleScreenRecorder (Linux)](https://maartenbaert.be/simplescreenrecorder)  

## Videobearbeitung

- [Openshot (Windows, MacOS, Linux)](https://openshot.org)
- [kdenlive (Windows, Linux)](https://kdenlive.org)  

## Audiobearbeitung

- [Audacity (Windows, MacOS, Linux)](https://audacityteam.org)  

## Bildbearbeitung

Für [Bildbearbeitungsprogramme gibt es eine eigene Kategorie in unserem Wiki](/wiki/programme/bildbearbeitung/).

## Kamera

- [OpenCamera (Android)](https://f-droid.org/packages/net.sourceforge.opencamera/)   
